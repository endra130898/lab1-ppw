from django.test import TestCase, Client
from django.urls import resolve
from .views import index, friend_list
from .api_csui_helper.csui_helper import CSUIhelper
from .models import Friend

# Create your tests here.
class Lab7UnitTest(TestCase):

	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)
		
	def test_lab_7_friend_list_url_is_exist(self):
		response = Client().get('/lab-7/friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_using_friend_list_func(self):
		found = resolve('/lab-7/friend-list/')
		self.assertEqual(found.func, friend_list)

	def test_lab_7_using_wrong_sso_username_or_password(self):
		self.assertRaises(Exception, CSUIhelper, username='wrong_username', password='wrong_password')

	def test_lab_7_csui_helper_get_auth_param_dict(self):
		csui_helper = CSUIhelper()
		auth_param_dict = csui_helper.instance.get_auth_param_dict()
		self.assertEqual(auth_param_dict['client_id'], "X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG")

	def test_lab_7_index_with_wrong_page_number(self):
		response = Client().get('/lab-7/?page=0')
		self.assertEqual(response.status_code, 302)

	def test_lab_7_friend_list_json(self):
		# create one friend
		new_friend = Friend.objects.create(friend_name='dummy_name', npm='dummy_npm')

		response= Client().get('/lab-7/get-friend-list/')

		self.assertEqual(response.status_code, 200)

	def test_lab_7_create_new_friend(self):
		response = Client().post('/lab-7/add-friend/', {'name': 'dummy_name', 'npm': 'dummy_npm'})

		self.assertEqual(response.status_code, 200)
		self.assertEqual(Friend.objects.all().count(), 1)

	def test_lab_7_create_new_friend_that_already_exist(self):
		# create one friend
		new_friend = Friend.objects.create(friend_name='dummy_name', npm='dummy_npm')

		response = Client().post('/lab-7/add-friend/', {'name': 'dummy_name', 'npm': 'dummy_npm'})

		self.assertEqual(response.status_code, 400)

	def test_lab_7_delete_friend(self):
		# create one friend
		new_friend = Friend.objects.create(friend_name='dummy_name', npm='dummy_npm')

		self.assertEqual(Friend.objects.all().count(), 1)

		response = Client().get('/lab-7/delete-friend/1/')

		self.assertEqual(Friend.objects.all().count(), 0)

	def test_lab_7_validate_npm(self):
		# create one friend
		new_friend = Friend.objects.create(friend_name='dummy_name', npm='dummy_npm')

		response = Client().post('/lab-7/validate-npm/', {'npm': 'dummy_npm'})

		self.assertEqual(response.status_code, 200)
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

# Create your views here.
response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada

    # mendapatkan halaman keberapa dari list mahasiswa
    page = request.GET.get('page', 1)
    try:
        page = int(page)
        mahasiswa_list = csui_helper.instance.get_mahasiswa_list(page)["results"]
    except:
        # tidak ada page atau page yang diberikan bukan integer
        return HttpResponseRedirect('/lab-7/')

    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
    response['author'] = 'Endrawan'
    response['prev'] = page - 1
    response['next'] = page + 1
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    response['author'] = 'Endrawan'
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request):
    # ubah semua friend menjadi list of dictionary
    data = []
    for friend in Friend.objects.all():
        data.append(model_to_dict(friend))
    return JsonResponse({'data': data})

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']

        # friend sudah ada di database
        if friend_already_exist(npm):
            return HttpResponse(status=400)

        #tambahkan friend baru
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        data = model_to_dict(friend)
        return HttpResponse(data)

def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponse(status=200)

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': friend_already_exist(npm) #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)[0]

    # data berisi field model dan idnya
    data = struct["fields"]
    data["id"] = struct["pk"]

    data = json.dumps(data)
    return data

def friend_already_exist(npm):
    return Friend.objects.filter(npm=npm).count() > 0
from http.cookies import SimpleCookie
from django.test import TestCase, Client
from django.urls import resolve
from .views import index
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class Lab9UnitTest(TestCase):

    def test_lab_9_url_is_exist(self):
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_using_index_func(self):
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)

    def test_lab_9_auth_login_wrong_username(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': 'wrong_username', 'password': 'wrong_password'})
        self.assertEqual(response.status_code, 302)

    def test_lab_9_auth_login_and_logout(self):
        # login
        response = self.client.post('/lab-9/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})

        # logout
        response = self.client.get('/lab-9/custom_auth/logout/')
        self.assertEqual(response.status_code, 302)

    def test_lab_9_index_after_login(self):
        # login
        response = self.client.post('/lab-9/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})

        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 302)

    def test_lab_9_profile_before_login(self):
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 302)

    def test_lab_9_profile_after_login(self):
        # login
        response = self.client.post('/lab-9/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})

        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_add_session_item(self):
        response = self.client.get('/lab-9/add_session_item/0/0/') # drones
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/lab-9/add_session_item/1/0/') # soundcards
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/lab-9/add_session_item/2/0/') # opticals
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/lab-9/add_session_item/0/1/') # another drones
        self.assertEqual(response.status_code, 302)

    def test_lab_9_del_session_item(self):
        # add
        response = self.client.get('/lab-9/add_session_item/0/0/')
        self.assertEqual(response.status_code, 302)

        # del
        response = self.client.get('/lab-9/del_session_item/0/0/')
        self.assertEqual(response.status_code, 302)

    def test_lab_9_clear_session_item(self):
        # add
        response = self.client.get('/lab-9/add_session_item/0/0/')
        self.assertEqual(response.status_code, 302)

        # clear
        response = self.client.get('/lab-9/clear_session_item/0/')
        self.assertEqual(response.status_code, 302)

    def test_lab_9_profile_fav_items(self):
        # login
        response = self.client.post('/lab-9/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})

        # add to fav
        response = self.client.get('/lab-9/add_session_item/0/0/') # drones
        response = self.client.get('/lab-9/add_session_item/1/0/') # soundcards
        response = self.client.get('/lab-9/add_session_item/2/0/') # opticals

        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_cookie_auth_login_get(self):
        response = self.client.get('/lab-9/cookie/auth_login/')
        self.assertEqual(response.status_code, 302)

    def test_lab_9_cookie_auth_login_wrong_username(self):
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'wrong_username', 'password': 'wrong_password'})
        self.assertEqual(response.status_code, 302)

    def test_lab_9_cookie_auth_login_correct_username(self):
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'utest', 'password': 'ptest'})
        self.assertEqual(response.status_code, 302)

    def test_lab_9_cookie_login_before_login(self):
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_cookie_login_after_login(self):
        # login
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'utest', 'password': 'ptest'})

        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 302)

    def test_lab_9_cookie_profile_before_login(self):
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 302)

    def test_lab_9_cookie_profile_after_login(self):
        # login
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'utest', 'password': 'ptest'})

        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_cookie_profile_hack_attempt(self):
        self.client.cookies = SimpleCookie({'user_login': 'wrong_username', 'user_password': 'wrong_password'})
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_cookie_clear(self):
        response = self.client.get('/lab-9/cookie/clear/')
        self.assertEqual(response.status_code, 302)
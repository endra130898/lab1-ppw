$(document).ready(function(){
	$('#msg-value').keypress(function (e) {
		if (e.which == 13) { // press enter
			var message = "<p class='msg-send'>" + $('#msg-value').val() + "</p>"
			$('#msg-value').val("") // reset
			$('.msg-insert').append(message)
			return false;
		}
	})
});

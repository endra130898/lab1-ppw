// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
	if (erase) { // erase the value
		print.value = "" // reset
		erase = false; // we have erased the value 
	}

  	if (x === 'ac') {
    	print.value = "" // reset
    } else if (x === 'log') {
    	print.value = Math.round(Math.log(print.value) * 10000) / 10000;
    } else if (x === 'sin') {
    	print.value = Math.round(Math.sin(print.value) * 10000) / 10000;
    } else if (x === 'tan') {
    	print.value = Math.round(Math.tan(print.value) * 10000) / 10000;
  	} else if (x === 'eval') {
    	print.value = Math.round(evil(print.value) * 10000) / 10000;
    	erase = true;
  	} else {
    	print.value += x;
  	}
};

function evil(fn) {
  	return new Function('return ' + fn)();
}


$(document).ready(function(){
	// chat
	$('#msg-value').keypress(function (e) {
		if (e.which == 13) { // press enter
			var message = "<p class='msg-send'>" + $('#msg-value').val() + "</p>"
			$('#msg-value').val("") // reset
			$('.msg-insert').append(message)
			return false;
		}
	})

	// create themes and save to local storage
	var themes = [
		{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
		{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
		{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
		{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
		{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
		{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
		{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
		{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
		{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
		{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
		{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];
	var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
	if (localStorage.selectedTheme) {
		// theme exist in local storage, so change color to it
		var lastTheme = JSON.parse(localStorage.selectedTheme)
		$('body').css('background-color', lastTheme['bcgColor'])
		$('body').css('color', lastTheme['fontColor'])
	} else {
		// theme do not exist in local storage
		localStorage.selectedTheme = JSON.stringify(selectedTheme);
		localStorage.themes = JSON.stringify(themes);
	}

	$('.my-select').select2({
		'data': JSON.parse(localStorage.themes)
	});

	$('.apply-button').on('click', function(){
    	var value = $('.my-select').val()

    	// find theme in themes
    	var allThemesInStorage = JSON.parse(localStorage.themes)
    	var themeToBeChanged = allThemesInStorage[value]

    	// change color and save to local storage
    	$('body').css('background-color', themeToBeChanged['bcgColor'])
    	$('body').css('color', themeToBeChanged['fontColor'])
    	localStorage.selectedTheme = JSON.stringify(themeToBeChanged)
})

});

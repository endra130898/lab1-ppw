from django.test import TestCase
from django.urls import resolve
from .views import index
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class Lab10UnitTest(TestCase):

    def test_lab_10_url_is_exist(self):
        response = self.client.get('/lab-10/')
        self.assertEqual(response.status_code, 200)

    def test_lab_10_using_index_func(self):
        found = resolve('/lab-10/')
        self.assertEqual(found.func, index)

    def test_lab_10_auth_login_wrong_username(self):
        response = self.client.post('/lab-10/custom_auth/login/', {'username': 'wrong_username', 'password': 'wrong_password'})
        self.assertEqual(response.status_code, 302)

    def test_lab_10_auth_login_and_logout(self):
        # login
        response = self.client.post('/lab-10/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})

        # logout
        response = self.client.get('/lab-10/custom_auth/logout/')
        self.assertEqual(response.status_code, 302)

    def test_lab_10_index_after_login(self):
        # login
        response = self.client.post('/lab-10/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})

        response = self.client.get('/lab-10/')
        self.assertEqual(response.status_code, 302)

    def test_lab_10_dashboard_before_login(self):
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 302)

    def test_lab_10_dashboard_after_login(self):
        # login
        response = self.client.post('/lab-10/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})

        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)

    def test_lab_10_dashboard_two_time_login(self):
        # login
        response = self.client.post('/lab-10/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})

        response = self.client.get('/lab-10/dashboard/')

        # logout
        response = self.client.get('/lab-10/custom_auth/logout/')

        # login
        response = self.client.post('/lab-10/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})

        response = self.client.get('/lab-10/dashboard/')

        self.assertEqual(response.status_code, 200)

    def test_lab_10_movie_list(self):
        response = self.client.get('/lab-10/movie/list/')

        response = self.client.get('/lab-10/movie/list/?judul=judul&tahun=2017')

        self.assertEqual(response.status_code, 200)

    def test_lab_10_movie_detail(self):
        # before login
        response = self.client.get('/lab-10/movie/detail/tt3606752/')
        # login
        response = self.client.post('/lab-10/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})
        response = self.client.get('/lab-10/dashboard/')
        # after login
        response = self.client.get('/lab-10/movie/detail/tt3606752/')

        self.assertEqual(response.status_code, 200)

    def test_lab_10_add_watch_later_before_login(self):
        response = self.client.get('/lab-10/movie/watch_later/add/tt3606752/')
        response = self.client.get('/lab-10/movie/watch_later/add/tt3606752/') # hacking attempt

        # another add
        response = self.client.get('/lab-10/movie/watch_later/add/tt7084926/')

        # login
        response = self.client.post('/lab-10/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})
        response = self.client.get('/lab-10/dashboard/')

        self.assertEqual(response.status_code, 200)

    def test_lab_10_add_watch_later_after_login(self):
        # login
        response = self.client.post('/lab-10/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})
        response = self.client.get('/lab-10/dashboard/')
        
        response = self.client.get('/lab-10/movie/watch_later/add/tt3606752/')
        response = self.client.get('/lab-10/movie/watch_later/add/tt3606752/') # hacking attempt

        self.assertEqual(response.status_code, 302)

    def test_lab_10_list_watch_later(self):
        # before login
        response = self.client.get('/lab-10/movie/watch_later/')

        # login
        response = self.client.post('/lab-10/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})
        response = self.client.get('/lab-10/dashboard/')
        # add movie
        response = self.client.get('/lab-10/movie/watch_later/add/tt3606752/')

        # after login
        response = self.client.get('/lab-10/movie/watch_later/')

        self.assertEqual(response.status_code, 200)

    def test_lab_10_add_watched(self):
        # login
        response = self.client.post('/lab-10/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})
        response = self.client.get('/lab-10/dashboard/')
        
        response = self.client.get('/lab-10/movie/watched/add/tt3606752/')
        response = self.client.get('/lab-10/movie/watched/add/tt3606752/') # hacking attempt

        self.assertEqual(response.status_code, 302)

    def test_lab_10_list_watched(self):
        # login
        response = self.client.post('/lab-10/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})
        response = self.client.get('/lab-10/dashboard/')
        # add movie
        response = self.client.get('/lab-10/movie/watched/add/tt3606752/')

        response = self.client.get('/lab-10/movie/watched/')

        self.assertEqual(response.status_code, 200)

    def test_lab_10_api_search_movie(self):
        response = self.client.get('/lab-10/api/movie/-/-/')

        # > 3 pages
        response = self.client.get('/lab-10/api/movie/justice/2016/')

        # 0 < x <= 3 pages
        response = self.client.get('/lab-10/api/movie/Guardians of Galaxy/2016/')

        # 0 pages
        response = self.client.get('/lab-10/api/movie/this_is_not_movie_name/2020/')

        self.assertEqual(response.status_code, 200)